function WriteAFile(fileName, content) {
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, 
        function(fileSystem) {
            fileSystem.root.getFile(fileName, {create : true, exclusive : false }, 
                function(fileEntry) {
                    fileEntry.createWriter(function(writer) {
                            writer.write(content);
                    }, failWriter);
                }, failWriter);
        },
        failWriter);
}

function failWriter(error) {
    console.log(error.code);
}

var results = new Array();
var totalCurrent;
var ts = 0;

function canvasFileTransferToNode(canvas, box, scheme, totalSends) {
    if (ts == 0) {
        ts = totalSends;
        totalCurrent = 0;
    }
    
    var fileURI = box.URI;
   // scheme = JSON.parse(scheme);
    
    console.log('SCHEME\n' + JSON.stringify(scheme));
    
    console.log("FILE URI " + box.URI);
    var options = new FileUploadOptions();
    options.fileKey = "file";
    options.mimeType = "text/plain";
    options.fileName = fileURI.substr(fileURI.lastIndexOf('/')+1);
    var params = {};
    params.ocr = scheme;
    params.name = canvas.id;
    //params.privado = true;
    options.params = params;
    var ft = new FileTransfer();
    ft.upload(fileURI, encodeURI("http://sl.wancharle.com.br/note/ocr"), FileTransferToNodeSucess, FileTransferToNodeFail, options);
}
              
function FileTransferToNodeSucess(r) {
    console.log("Code = " + r.responseCode);
    console.log("Response = " + r.response);
    console.log("Sent = " + r.bytesSent);
    results[totalCurrent] = JSON.parse(r.response);
    totalCurrent++;
    if (totalCurrent == ts) {
        ts = 0;
        ProcessExtractedData(results);
    }
}
    
function FileTransferToNodeFail(error) {
    console.log(error);
    console.log("upload error source " + error.source);
    console.log("upload error target " + error.target);
}