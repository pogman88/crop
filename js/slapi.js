var SLAPI_LOGADO = false;
var SLAPI_ENVIADO = false;

function SLapiOk(id){
    if (desenvolvedor == true) {
        console.log("Configuração OK.");
    }
    
    SLapi.user.login('fernando', '123456');
    SLapi.on(SLSAPI.User.EVENT_LOGIN_FINISH, function(err) {
        if (desenvolvedor == true) {
            console.log("Login OK");
        }
        SLAPI_LOGADO = true;
    });

}
function SLapiFail(origin, test){
    ErrorSLapi(origin);   
}

function SLapiEnviar(jsonScheme){
    if (SLAPI_LOGADO == true) {
        json = {}    
        
        json.user = SLapi.user.user_id;
        if (position != null) {
            json.latitude = position.coords.latitude;
            json.longitude = position.coords.longitude;
        } else {
            json.latitude = 0.0;
            json.longitude = 0.0;
        }
        json.fotoURI = photoURL;
        
        console.log("ENVIANDO FOTO...");
        
        // Para enviar a foto
        SLapi.notes.enviar(json, "55bfc4d3855f711876ee062e", function(r) {
            SLapiNotaEnviada(r, jsonScheme);
        }, function(test) {
            SLapiFail('submit', test);   
        });
    }
}

var timeOcrResult;
var jsonOcrResult;
function SLapiNotaEnviada(jsonResposta, jsonScheme){
    // funcao chamada quando uma nota é enviada com sucesso     
    if (jsonResposta) {
        console.log(JSON.parse(jsonResposta.response));
        var id = SLapiID = JSON.parse(jsonResposta.response).id;
        console.log("FOTO SALVA");
        console.log("ENVIANDO SCHEME...");
        dados = {ocr_validated : jsonScheme};
        SLapi.notes.update(id, dados, function(r) { 
            console.log("SCHEME SALVO.");
        }, function(test) {
            SLapiFail('submit', test);   
        });
        
    } else { // bug android
        console.log("RESPOSTA NULA");
    }
}

function SLapiAtualizarNota(jsonScheme) {
    var id = SLapiID;
    SLapi.notes.update(id, jsonScheme, function(r) {
        timeOcrResult++;
        jsonOcrResult[i] = r;

        if (timeOcrResult == totalSelection) {
            SLAPI_ENVIADO = true;
            ProcessExtractedData(jsonOcrResult);
        }
    },  function(test) {
        SLapiFail('update', test);   
    });
}

var conf;
conf = {
  "user": {
    "username": "wan",
    "password": "$2a$10$eaxlYfvFjmJGwFSe5dSkLOKTyOTqJnNiSJYwvyLQY9swsjpgrSlvu",
    "createdAt": "2015-03-26T17:43:46.612Z",
    "updatedAt": "2015-03-26T17:43:46.612Z",
    "id": "55144552ccf4795e0ad92f78"
  },
  "title": "CropFernando",
  "createdAt": "2015-05-28T22:11:23.334Z",
  "updatedAt": "2015-06-15T20:37:12.854Z",
  "dataSources": [
    {
      "url": "http://sl.wancharle.com.br/note/lista?notebook=55bfc4d3855f711876ee062e",
      "func_code": "function (item){ return item; }"
    }
  ],
  "storageNotebook": {
    "name": "cropfernando",
    "createdAt": "2015-08-03T19:45:23.919Z",
    "updatedAt": "2015-08-03T19:45:23.919Z",
    "id": "55bfc4d3855f711876ee062e"
  },
  "notdelete": "true",
  "id": "5567928b95b248224048e516",
  "container_id": "messages"
}

var SLapi = new SLSAPI(conf);

// registro de eventos
SLapi.on(SLSAPI.Config.EVENT_READY, SLapiOk);
SLapi.on(SLSAPI.Config.EVENT_FAIL, SLapiFail);
SLapi.on(SLSAPI.Notes.EVENT_ADD_NOTE_FINISH, SLapiNotaEnviada);

if (desenvolvedor == true) {
    console.log(SLapi);
}