function Box() {
	this.idSelection;
	this.type;
    this.value;
	this.x;
	this.y;
	this.width;
	this.height;
    this.validated;
	this.name;
    this.confidence;
    this.URI;
    
	this.Init = function(id, coordinates) {
		this.idSelection = id;
		this.type = msgs_type_text; // Default value for type is text
		this.path = "";
        this.value = "";
		this.x = coordinates.x;
		this.y = coordinates.y;
		this.width = coordinates.w;
		this.height = coordinates.h;
        this.validated = false;
        this.confidence = -1;
        this.URI = "";
        this.name = "";
	}
    
    this.IsInit = function() {
        return this.isInit;   
    }
	
	this.Update = function(type, path, value, validated, coordinates) {
		var ret = false;
		
		if (type !== null) {
			this.type = type;
		}
		if (path !== null) {
			this.path = path;
		}
        if (value !== null) {
            this.value = value;   
        }
        if (validated !== null) {
            this.validated = validated;   
        }
		if (coordinates !== null) {
			if (this.x === coordinates.x &&
				this.y === coordinates.y &&
				this.width === coordinates.w &&
				this.height === coordinates.h) {
			}
			this.x = coordinates.x;
			this.y = coordinates.y;
			this.width = coordinates.w;
			this.height = coordinates.h;
		}
	}
	
	this.ToString = function() {
        var json = '"type": "' + this.type + '",';
        
        if (this.name != null) {
            json += '"name": "' + this.name + '",';   
        }
        
        var v = 0;
        if (this.validated == true) {
            v = 1;
        }
        
		return json + 
               '"path": "box"' + this.idSelection + '",' +
               '"validated": "' + v + '",' +
               '"value": "' + this.value + '",' +
               '"box": {' +
			   '     "x": "' + this.x + '",' +
			   '     "y": "' + this.y + '",' +
			   '     "width": "' + this.width + '",' +
			   '     "height": "' + this.height + '"' +
               '}';
	}
}

// Associative Array
var boxes;
var cb;
var desenvolvedor = true;
var DEFAULT_CONFIDENCE = 75;
var totalSelection = 0;    
var beginMove = false;
var realWidth;
var realHeight;
var clientWidth;
var clientHeight;
var blur = true;
var processData = false;
var speed = 60;
var inAnimation = false;
var adjustImg = false
var photoTaken = false;
var MIN_SELECTION_WIDTH = 10;
var MIN_SELECTION_HEIGHT = 10;
var position = null;
var targetCanvas = null;
var totalSends = 0;

// Dados de serviddor
var jsonScheme = null;
var photoURI_Extraction = "";
var photoURL = "";
var SLapiID = "";

jQuery(window).load($(function(){
    document.addEventListener('deviceready', onDeviceReady, false);
    
    $('#clearSelection').on('click', function(e) {
        ClearSelection();
    });

    $('#submit').on('click', function() {
        var m = cb.ui.multi;
        if (m.length == 0) {
            AutomaticSelection(); 
        } else {
            var totalFalse = 0;
            var m = cb.ui.multi;
            for (var i = 0; i < m.length; i++) {
                if (m[i].cropBox.validated == false) {
                    totalFalse++;
                }
            }
            
            if (totalFalse == 0) {
                // Perguntar se deseja salvar os validos
                document.getElementById('submit').setAttribute('href', '#popupSaveAllValidateds');
            } else if (totalFalse < totalSelection) {
                // Perguntar se deseja extrair os invalidos ou salvar os validos
            } else {
                // Perguntar se deseja extrair os invalidos
                document.getElementById('submit').setAttribute('href', '#popupFilters');
            }
        }
    });
    
    $('#popupFiltersSubmit').on('click', function() { 
        Submit();
    });

    $('#takePhoto').on('click', function() {
        onTakePicture();
    });

    $('#popupDialogLink').on('click', function() {
        showPopupDialog();
    });

    $('#popupDialogBack').on('click', function() {
        StartSelection(cb.ui.selection);
    });

    $('#scrollUp').on('click', function() {
        ScrollScreen(-speed); 
    });

    $('#scrollDown').on('click', function() {
        ScrollScreen(speed); 
    });

    $('#ToCamera').on('click', function() {
        onStartCamera();   
    });
    
    $('#search').on('click', function() {
        onSearchPhoto();
    });

    $('#BackCamera').on('click', function() {
        onStopCamera();   
    });

    $('#zoomIn').on('click', function() {
        onZoomIn();   
    });

    $('#zoomOut').on('click', function() {
        onZoomOut();   
    });
    
    function initJCROP() {
        var $targ = $('#target');
        $targ.Jcrop({
            bgOpacity: .35,
            addClass: 'jcrop-light',
            bgFade: .10,
            multi: true,
            canResize: true,
            canDrag: true,
            canSelect: true,
            canDelete: true,
            allowSelect: true
        },function(){
            crop_image_load(this);
        });
    }
    
    function onDeviceReady() {
        pictureSource=navigator.camera.PictureSourceType;
        destinationType=navigator.camera.DestinationType;
        
        //on picture
        cordova.plugins.camerapreview.setOnPictureTakenHandler(function(result){        
            document.getElementById('target').src = result[1];
            photoURI_Extraction = result[1];
            initJCROP();
            $.mobile.changePage($('#crop'));
            showAjax('Realizando Seleção automática...');
            photoTaken = true;
            setTimeout(function() {
                AutomaticSelection();
                hideAjax();
            }, 500);
        });
        
        jQuery(document).bind('searchPhotoEvent', function() {
            document.getElementById('target').src = photoURI_Extraction;
            initJCROP();
            $("#submit").html('Seleção automática');
            $.mobile.changePage($('#crop'));
        });
        
        jQuery(document).bind('loadScheme', function(jsonScheme) {
            var photoURL = jsonScheme.fotoURL;
            document.getElementById('target').src = photoURL;
            initJCROP();
            jQuery(document).trigger('startSelectionFromScheme', jsonScheme);
        });
        
        jQuery(document).bind('startSelectionFromScheme', function(jsonScheme) {
            AutomaticSelectionWithScheme(jsonScheme);
        });
        
        navigator.geolocation.getCurrentPosition(function (pos)  {
            position = pos;
        });
    };

    function crop_image_load(obj) {
        cb = obj;
        boxes = new Array();

        cb.container.on('cropend', function(e, s, c) {
            if (desenvolvedor == true) {
                console.log("EVENTO CROPEND");
            }

            EndSelection(s);
        });

        cb.container.on('cropcreate', function(e, s) {
            if (desenvolvedor == true) {
                console.log("EVENTO CROPCREATE");
            }

            CreateSelection();
        });

        cb.container.on('cropstart', function(e, s, c) {
            if (desenvolvedor == true) {
                console.log("EVENTO CROPSTART");
            }

            StartSelection(s);
        });    

        cb.container.on('cropmove', function(e, s, c) {
            if (beginMove == false) {
                return;   
            }

            if (desenvolvedor == true) {
                console.log("EVENTO CROPMOVE");
            }

            // Checar sobreposição
            MoveSelection(s, c);
        });

        $( document ).on( "click", ".show-page-loading-msg", function() {
            var $this = $( this ),
                theme = $this.jqmData( "theme" ) || $.mobile.loader.prototype.options.theme,
                msgText = $this.jqmData( "msgtext" ) || $.mobile.loader.prototype.options.text,
                textVisible = $this.jqmData( "textvisible" ) || $.mobile.loader.prototype.options.textVisible,
                textonly = !!$this.jqmData( "textonly" );
                html = $this.jqmData( "html" ) || "";
            $.mobile.loading( "show", {
                    text: msgText,
                    textVisible: textVisible,
                    theme: theme,
                    textonly: textonly,
                    html: html
            });
        })
        
        adjustWidthHeightImg(); 
    }
}));

$( '#crop' ).on( 'pagebeforeshow', function(event){
    $("#idSelection").parent().hide(); //hide the newly jqm-created div around my input
});

function AutomaticSelectionWithScheme(scheme) {
    jsonScheme = scheme;
    
    var ocr_validated = jsonScheme.ocr_validated;
    
    for (var i = 0; i < ocr_validated.fields.length; i++) {
        var cropBox = ocr_validated.fields[i];
        var selection = cb.newSelection().update(
            $.Jcrop.wrapFromXywh([cropBox.box.x, cropBox.box.y, cropBox.box.width, cropBox.box.height])    
        );
        
        var box = new Box();
        var path = cropBox.path;
        var id = path.split("box")[1];
        box.Init(parseInt(id), s.get());
        selection.cropBox = box;
        
        StartSelection(selection);
    }
    
    totalSelection = cb.ui.multi.length;
    SLAPI_ENVIADO = true;
    
    $("#submit").html('Enviar dados');
}

function AutomaticSelection() {
    var canvas = document.getElementsByTagName('canvas');
    canvas = canvas[0];
    console.log(canvas.clientHeight);
    console.log(canvas.clientWidth);
    
    console.log(canvas.height);
    console.log(canvas.width);
    
    // Nome
    var x = 0;
    var width = canvas.clientWidth - x;
    
    var y = 21 * canvas.clientHeight / 100;
    var height = (68 * canvas.clientHeight / 100) - y;
    
    var selection = cb.newSelection().update(
        $.Jcrop.wrapFromXywh([x, y, width, height])    
    );
    StartSelection(selection);
    
    // Valor total da Obra
    x = 6 * canvas.clientWidth / 100;
    width = (52 * canvas.clientWidth / 100) - x;
    
    y = 70 * canvas.clientHeight / 100;
    height = (73.5 * canvas.clientHeight / 100) - y;
    
    selection = cb.newSelection().update(
        $.Jcrop.wrapFromXywh([x, y, width, height])    
    );
    StartSelection(selection);
    
    // Comunidade
    x = 6 * canvas.clientWidth / 100;
    width = (52 * canvas.clientWidth / 100) - x;
    
    y = 73.5 * canvas.clientHeight / 100;
    height = (78.5 * canvas.clientHeight / 100) - y;
    
    selection = cb.newSelection().update(
        $.Jcrop.wrapFromXywh([x, y, width, height])    
    );
    StartSelection(selection);
    
    // Municipio
    x = 6 * canvas.clientWidth / 100;
    width = (52 * canvas.clientWidth / 100) - x;
    
    y = 78.5 * canvas.clientHeight / 100;
    height = (82.5 * canvas.clientHeight / 100) - y;
    
    selection = cb.newSelection().update(
        $.Jcrop.wrapFromXywh([x, y, width, height])    
    );
    StartSelection(selection);
    
    // Objeto
    x = 6 * canvas.clientWidth / 100;
    width = (52 * canvas.clientWidth / 100) - x;
    
    y = 82.5 * canvas.clientHeight / 100;
    height = (86.5 * canvas.clientHeight / 100) - y;
    
    selection = cb.newSelection().update(
        $.Jcrop.wrapFromXywh([x, y, width, height])    
    );
    StartSelection(selection);
    
    // Agentes Participantes
    x = 52 * canvas.clientWidth / 100;
    width = (97 * canvas.clientWidth / 100) - x;
    
    y = 70 * canvas.clientHeight / 100;
    height = (78.5 * canvas.clientHeight / 100) - y;
    
    selection = cb.newSelection().update(
        $.Jcrop.wrapFromXywh([x, y, width, height])    
    );
    StartSelection(selection);
    
    // Inicio da Obra
    x = 52 * canvas.clientWidth / 100;
    width = (97 * canvas.clientWidth / 100) - x;
    
    y = 78.5 * canvas.clientHeight / 100;
    height = (82.5 * canvas.clientHeight / 100) - y;
    
    selection = cb.newSelection().update(
        $.Jcrop.wrapFromXywh([x, y, width, height])    
    );
    StartSelection(selection);
    
    // Termino da Obra
    x = 52 * canvas.clientWidth / 100;
    width = (97 * canvas.clientWidth / 100) - x;
    
    y = 82.5 * canvas.clientHeight / 100;
    height = (86.5 * canvas.clientHeight / 100) - y;
    
    selection = cb.newSelection().update(
        $.Jcrop.wrapFromXywh([x, y, width, height])    
    );
    StartSelection(selection);
    
    $("#submit").html('Extrair dados');
}

function saveTakenPicture(canvas) {
    window.canvas2ImagePlugin.saveImageDataToLibrary(
        function(succesMessage) {
            console.log(succesMessage);
            photoURI_Extraction = succesMessage;
        }, 
        function(failMessage) {
            console.log(failMessage);
        },
        canvas
    );
}

function Submit() {
    $.mobile.changePage('#extraction', { transition: "fade", changeHash: false });
    showAjax('Processando...');
    
    blur = true;
    
    // Ajustar os tamanhos das seleções para o tamanho real da foto
    adjustWidthHeightBoxes();
    
    // Atualizar Campos
    var inputIdSelection = document.getElementById('idSelection');
    var inputType = $('input[name=radio-type]:checked');
    var inputValue = document.getElementById('value');
    if (inputIdSelection.value >= 0) {
        var i = FindIdBox(inputIdSelection.value);
        if (i > -1) {
            var os = cb.ui.multi[i];
            os.cropBox.Update(inputType.val(), null, inputValue.value, null, null);
        }
    }

    var needExtraction = false;
    var m = cb.ui.multi;
    for (var i = 0; i < m.length; i++) {
        if (m[i].cropBox.validated == false) {
            needExtraction = true;
            break;
        }
    }
 
    if (jsonScheme == null) {
        jsonScheme = '{' +
                        '"page": { ' +
                            '"x": 0 ,' +
                            '"y": 0 ,' +
                            '"width": ' + realWidth + ',' +
                            '"height":' + realHeight + ',' +
                            '"totalSelection": ' + 0 + 
                        '},' +
                        '"words": []' +
                     '}';
        
        console.log('JSON SCHEME CRIADO\n' + jsonScheme);
        jsonScheme = JSON.parse(jsonScheme);
        photoURL = photoURI_Extraction;
        
        // Chamar API Node-fv
        //SLapiEnviar(jsonScheme);
    }
    
    if (needExtraction == true) {
        
        var filter = $('input[name=radios-filters]:checked').val();
        
         if (targetCanvas == null) {
            targetCanvas = $('<canvas/>', {
                                id: 'targetCanvas'
                            }).prop({
                                width: realWidth,
                                height: realHeight
                            });

            $('#hiddenCanvas').append(targetCanvas);
             
            var img = $('#target')[0];
            var tC = document.getElementById('targetCanvas');
            var context = tC.getContext('2d');
            context.drawImage(img, 0, 0, realWidth, realHeight, 0, 0, tC.width, tC.height);
        }
        
        setTimeout(function () {
            var canvas = document.getElementById('targetCanvas');
            CanvasPixelManipulation(canvas, filter);

            createCanvas();
        }, 3000);
    } else {
        var newFields = '[';
        var m = cb.ui.multi;
        for (var i = 0; i < m.length; i++) {
            box = m[i].cropBox;
            if (box.validated == true) {
                newFields += '{' +
                                '"path": ' + box.path + ',' +
                                '"value": ' + box.value + ',' +
                                '"type": ' + box.type + ',' +
                                '"box" : { '
                                    '"x" : ' + box.x + ',' +
                                    '"y" : ' + box.y + ',' +
                                    '"width" : ' + box.width + ',' +
                                    '"height" : ' + box.height +
                                '}' +
                             '}';
                if (i + 1 < m.length) {
                    newFields += ',';
                }
            }
        }
        newFields += ']';
        newFields = JSON.parse(newFields);
        jsonScheme.fields = newFields;
    }
}

function SendToExtract() {
    console.log("Enviando para extração");
    var m = cb.ui.multi;
    for(var i = 0; i < m.length; i++) {
        var box = m[i].cropBox;
        console.log(box);
        if (box.validated == false) {
            var canvas = document.getElementById('box' + box.idSelection + '');
            console.log(box);
            var scheme = GenerateJSONS(box);
            canvasFileTransferToNode(canvas, box, scheme, totalSends);
        }
    }
}

function GenerateJSONS(box) {    
    var json = '{' +
                       '"page": { ' +
                          '"x": 0 ,' +
                          '"y": 0 ,' +
                          '"width": ' + box.width + ',' +
                          '"height":' + box.height +
                       '},';
    json += '"words": [],' +
            '"fields": [ {' +    
            '"type": "' + box.type + '",' +
            '"name": "' + box.name + '",' +
            '"path": "box' + box.idSelection + '",' +
            '"box": {' +
                    '"x": 0,' +
                    '"y": 0,' +
                    '"width": ' + box.width + ',' +
                    '"height":' + box.height +
            '}}]}';
        
    return JSON.parse(json);
}
function createCanvas() { 
    var m = cb.ui.multi;
    totalSends = 0;
    for(var i = 0; i < m.length; i++) {
        var box = m[i].cropBox;
        if (box.validated == false) {
		    var newCanvas = $('<canvas/>', {
		                        id: 'box' + box.idSelection + ''
		                    }).prop({
		                        width: box.width,
		                        height: box.height
		                    });
		    
		    $('#hiddenCanvas').append(newCanvas);
            totalSends++;
        }
        
    }
    
    passSelectionsToCanvas();
    setTimeout(function () {
        passCanvasToPng(0);
    }, 1000 * totalSends);
}

function passSelectionsToCanvas() {
    var m = cb.ui.multi;
    
    for(var i = 0; i < m.length; i++) {
        var box = m[i].cropBox;
        if (box.validated == false) {
            //var img = $('#target')[0];
            var img = document.getElementById('targetCanvas');
            var canvas = document.getElementById('box' + box.idSelection + '');
            var context = canvas.getContext('2d');
            context.drawImage(img, box.x, box.y, box.width, box.height, 0, 0, canvas.width, canvas.height);
        }
    }
}

// Bug para salvar os canvas como png. Caso não espere 1seg, apenas um canvas é salvo
function bug(i) {
    setTimeout(function() {
        passCanvasToPng(i);
    }, 1000);
}

function passCanvasToPng(i) {
    var m = cb.ui.multi;
    if (i >= m.length) {
        console.log('ACABOU');
        SendToExtract()
        return;
    }

    var box = m[i].cropBox;
    if (box.validated == false) {
        var canvas = document.getElementById('box' + box.idSelection + '');
        window.canvas2ImagePlugin.saveImageDataToLibrary(
            function(succesMessage) {
                console.log(box.idSelection);
                console.log(succesMessage);
                box.URI = succesMessage;
                bug(i+1);
            }, 
            function(failMessage) {
                console.log(failMessage);
                bug(i+1);
            },
            canvas
        );
    } else {
        bug(i+1);   
    }
    
    console.log('OPA');
}

//function GenerateJSONS(box) {
//    var m = cb.ui.multi;
//    
//    var json = '{' +
//                       '"page": { ' +
//                          '"x": 0 ,' +
//                          '"y": 0 ,' +
//                          '"width": ' + realWidth + ',' +
//                          '"height":' + realHeight +
//                       '},';
//    json += '"words": [],' +
//            '"fields": [';
//    
//    for (var i = 0; i < m.length; i++) {
//        var box = m[i].cropBox;
//        
//        // Caso o campo de validado seja false, então extrair
//        if (box.validated == false) {
//            json += '{';
//
//            if (box.type != null) {
//                json += '"type": "' + box.type + '",';
//            }
//
//            if (box.name != null) {
//                json += '"name": "' + box.name + '",';   
//            }
//
//            json += '"path": "box' + box.idSelection + '",' +
//                    '"box": {' +
//                        '"x": ' + box.x +',' +
//                        '"y": ' + box.y + ',' +
//                        '"width": ' + box.width + ',' +
//                        '"height":' + box.height +
//                   '}}';
//        }
//        
//        if (i + 1 < m.length) {
//            json +=',';
//        }
//    }
//    json += ']}';
//        
//    return json;
//}

function ProcessExtractedData(jsons) {
    if (desenvolvedor == true) {
        console.log("Iniciando processo pós-extração"); 
    }
    
    console.log(jsons);
    console.log(jsons.length);
    processData = true;
    
    // Iterar por todos os jsons
    for (var i = 0; i < jsons.length; i++) {
        console.log("AEEE");
        var result = jsons[''+ i + ''];
        var string = JSON.stringify(result);
        console.log(string);        
        console.log(result);
        var id = string.split('"')[1];
        id = id.split('box')[1];
        console.log(id);
        var box = cb.ui.multi[FindIdBox(parseInt(id))];
        var cropBoxResult = result['box' + id];
        
        box.cropBox.confidence = cropBoxResult.confidence;
        box.cropBox.value = cropBoxResult.value;
        
        console.log(cropBoxResult.value);
        console.log(box.cropBox.value);
        console.log(cropBoxResult.confidence);
        console.log(box.cropBox.confidence);

        // Validade da extração para o campo
        if (box.cropBox.confidence > DEFAULT_CONFIDENCE) {
            box.element.addClass("jcrop-box-valided");
            box.cropBox.validated = true;
        } else {
            box.element.addClass("jcrop-box-notValided");
            box.cropBox.validated = false;
        }

        // Ajustes no tamanho da fonte
        var tamPX = 12;

        // Mostrando o valor extráido
        box.element[0].children[0].setAttribute('style', 'font-size:' + tamPX + 'px;');
        box.element[0].children[0].textContent = box.cropBox.value;
    }
    
    if (desenvolvedor == true) {
        console.log("Fim processo pós-extração.\n Retornando a pagina de crop.");   
    }

    // Retornar a pagina de crop
    $.mobile.changePage('#crop', { transition: "fade", changeHash: false });
}

function ErrorSLapi(origin) {
    if (origin == 'submit') {
        document.getElementById('popupDialogLink').setAttribute('href', '#popupErrorSubmit');    
    } else if (origin == 'update') {
        document.getElementById('popupDialogLink').setAttribute('href', '#popupErrorUpdate');       
    }
    
    $('#popupDialogLink').click();
    
    console.log('ERROR SLAPI');
}

function CanvasPixelManipulation(canvas, filter) {
    var thresholding, inversion, contrast;
    
    // configuração do filtro a ser aplicado a imagem
    if (filter == 'white') {
        thresholding = -1;
        inversion = 255;
        contrast = 70;
    } else if(filter == 'color') {
        inversion = 255;
        contrast = 70;
        thresholding = 100;
    } else if (filter == 'black') {
        thresholding = -1;
        contrast = 100;
        inversion = -1;
    } else {
        thresholding = -1;
        inversion = -1;
        contrast = -1;
    }
    
    if (inversion != -1 || contrast != -1) {
        var context = canvas.getContext('2d');
        var imageData = context.getImageData(0,0, canvas.width, canvas.height);
        var data = imageData.data;
        
        if (thresholding == -1) {
            // Descolorir
            for (var i = 0; i < data.length; i+= 4) {
                var r = data[i];
                var g = data[i+1];
                var b = data[i+2];
                //var avg = (data[i] + data[i+1] + data[i+2]) / 3;
                var luminance = 0.2126*r + 0.7152*g + 0.0722*b;
                data[i] = luminance;
                data[i+1] = luminance;
                data[i+2] = luminance;
            }
        } else {
            // Thresholding
            for (var i=0; i<data.length; i+=4) {
                var r = data[i];
                var g = data[i+1];
                var b = data[i+2];
                // CIE luminance for the RGB
                // The human eye is bad at seeing red and blue, so we de-emphasize them.
                var luminance = 0.2126*r + 0.7152*g + 0.0722*b;
                
                if (luminance < thresholding) {
                    data[i] = data[i+1] = data[i+2] = 0;   
                } else {
                    data[i] = data[i+1] = data[i+2] = 255;
                }
            }
        }

        // Inverter preto e branco
        if (inversion > 0) {
            for (var i = 0; i < data.length; i+= 4) {
                data[i] = inversion - data[i];
                data[i+1] = inversion - data[i+1];
                data[i+2] = inversion - data[i+2];
            }
        }

        // Mudar Contraste
        if (contrast > 0) {
            var factor = (259 * (contrast + 255)) / (255 * (259 - contrast));

            for(var i=0;i<data.length;i+=4)
            {
                data[i] = factor * (data[i] - 128) + 128;
                data[i+1] = factor * (data[i+1] - 128) + 128;
                data[i+2] = factor * (data[i+2] - 128) + 128;
            }
        }
        
        context.putImageData(imageData, 0, 0);
    }
}

function ScrollScreen(speed) {
    if (inAnimation == false) {
        inAnimation = true;
        var y = $(window).scrollTop(); 
        $("html, body").animate({ scrollTop: y + speed }, 600);
        inAnimation = false;
    }
}

/**
    Corrige o tamanho da imagem do crop
**/
function adjustWidthHeightImg() {
   if (adjustImg == false) {
       adjustImg = true;

       var jcropActive = document.getElementsByClassName('jcrop-active jcrop-touch');
       jcropActive[0].setAttribute('style', 'width:100%;');

       var canvas = document.getElementsByTagName('canvas');    
       canvas[0].setAttribute('style', 'width:100%;');
    }
}

/**
    Corrige o tamanho da imagem das areas selecionadas
**/
function adjustWidthHeightBoxes() {    
    var canvas = document.getElementsByTagName('canvas'); 
    
    realWidth = canvas[0].width;
    realHeight = canvas[0].height;
    clientWidth = canvas[0].clientWidth;
    clientHeight = canvas[0].clientHeight;
    
    var m = cb.ui.multi;
    for (var i = 0; i < m.length; i++) {
        var box = m[i].cropBox;
        box.x = realWidth * box.x / clientWidth;
        box.y = realHeight * box.y / clientHeight;
        
        box.width = realWidth * box.width / clientWidth;
        box.height = realHeight * box.height / clientHeight;
    }

    console.log("REAL WIDTH " + realWidth);
    console.log("REAL HEIGHT " + realHeight);
    console.log("CLIENT WIDTH " + clientWidth);
    console.log("CLIENT HEIGHT " + clientHeight);
}

function showPopupDialog() {
    var s = cb.ui.selection;
    
    if (s == null || blur == true) {
        document.getElementById('popupDialogLink').setAttribute('href', '#popupBasic');    
        return;   
    }
    
    document.getElementById('popupDialogLink').setAttribute('href', '#popupDialog');   
}

function ClearSelection() {
    if (cb.ui.multi.length > 1) {
        if (desenvolvedor == true) {
            console.log("CLEAR SELECTION: Campo " + cb.ui.selection.cropBox.idSelection + " excluido.");
        }
        //cb.requestDelete(); 
        if (desenvolvedor == true) {
            console.log("SELECTION: Campo " + cb.ui.selection.cropBox.idSelection + " selecionado.");
        }
        //totalSelection--;
    }
}

function FindIdBox(id) {
    var m = cb.ui.multi;
    for (var i = 0; i < m.length; i++) {
        var box = m[i].cropBox;
        if (box.idSelection == id) {
            return i;   
        }
    }

    return -1;
}

/**
    Function
    Params: selection and coordinates
    Description: check if a selection overlapping a previous selection 
     (we computing the Minkowski sum of B and A for do this
     source: http://gamedev.stackexchange.com/questions/29786/a-simple-2d-rectangle-collision-algorithm-that-also-determines-which-sides-that)
    Return: void
**/
function MoveSelection(selection, coordinates) {
    var m = cb.ui.multi;

    // Verifica se existe mais de uma seleção
    if (m.length === 1) {
        return;
    }

    // Caso exista mais de uma seleção, verificar sobreposição
    for (var i = 1; i < m.length; i++) {
        var p = m[i].get();
        var s = selection.get();

        var w = 0.5 * (s.w + p.w);
        var h = 0.5 * (s.h + p.h);
        var dx = (s.x + s.w/2) - (p.x + p.w/2);
        var dy = (s.y + s.h/2) - (p.y + p.h/2);

        if (Math.abs(dx) <= w && Math.abs(dy) <= h)
        {
            /* collision! */
            var wy = w * dy;
            var hx = h * dx;
            if (wy > hx) {
                if (wy > -hx) {
                    /* collision at the top */
                    selection.moveTo(s.x, p.y + p.h);
                    selection.resize(s.w, s.h - (p.y + p.h - s.y));
                } else {
                    /* on the left */
                    selection.resize(p.x - s.x, s.h);
                }
            } else {
                if (wy > -hx) {
                    /* on the right */
                    selection.moveTo(p.x + p.w, s.y);
                    selection.resize(s.w - (p.x + p.w - s.x), s.h);
                } else {
                    /* at the bottom */
                    selection.resize(s.w, p.y - s.y);
                }
            }
        }
    }
}

function CreateSelection() {

}

function StartSelection(s) { 
    
    if (photoTaken == true) {
        saveTakenPicture(document.getElementsByTagName('canvas')[0]);
        photoTaken = false;
    }
    
    if (s.cropBox == null) {
        var box = new Box();
        box.Init(totalSelection, s.get());
        s.cropBox = box;
        totalSelection++;
    }

    // Atualizar Campos
    var inputIdSelection = document.getElementById('idSelection');
    var inputType = $('input[name=radio-type]:checked');
    var inputValue = document.getElementById('value');
    var inputValidate = $('input[name=radio-validated]:checked');

    if (inputIdSelection.value >= 0 && processData == false) {
        var i = FindIdBox(inputIdSelection.value);
        if (i > -1) {
            var os = cb.ui.multi[i];
            if (inputValidate.val() == "yes") {
                os.cropBox.Update(inputType.val(), null, inputValue.value, true, null);
            } else {
                os.cropBox.Update(inputType.val(), null, inputValue.value, false, null);    
            }
        }
    }

    $('input[name=radio-type][value=text]').prop('checked', false).checkboxradio('refresh');
    $('input[name=radio-type][value=number]').prop('checked', false).checkboxradio('refresh');

    $('input[name=radio-validated][value=yes]').prop('checked', false).checkboxradio('refresh');
    $('input[name=radio-validated][value=no]').prop('checked', false).checkboxradio('refresh');

    inputIdSelection.value = s.cropBox.idSelection;
    console.log('ID ' + inputIdSelection.value);
    $('input[name=radio-type][value=' + s.cropBox.type + ']').prop('checked', true).checkboxradio('refresh');
    inputValue.value = s.cropBox.value;
    
    if (s.cropBox.validated == true) {
        $('input[name=radio-validated][value=yes]').prop('checked', true).checkboxradio('refresh');
        if (SLAPI_ENVIADO == true && s.cropBox.confidence >= 0) {
            s.element.removeClass("jcrop-box-notValided");
            s.element.addClass("jcrop-box-valided");
            $('#radios-validated input[type="radio"]').checkboxradio('enable');
            $('#value').textinput('enable');
        } else {
            $('#radios-validated input[type="radio"]').checkboxradio('disable');
        }
    } else {
        $('input[name=radio-validated][value=no]').prop('checked', true).checkboxradio('refresh');
        if (SLAPI_ENVIADO == true && s.cropBox.confidence >= 0) {
            s.element.removeClass("jcrop-box-valided");
            s.element.addClass("jcrop-box-notValided");
            $('#radios-validated input[type="radio"]').checkboxradio('enable');
            $('#value').textinput('enable');
        } else {
            $('#radios-validated input[type="radio"]').checkboxradio('disable');
        }
    }
    
    
    var tamPX = 11;

    s.element[0].children[0].setAttribute('style', 'font-size:' + tamPX + 'px;');
    s.element[0].children[0].textContent = s.cropBox.value;

    beginMove = true;
    processData = false;
}

 function EndSelection(s) {
    beginMove = false;
    blur = false;
    var s = cb.ui.selection;
    console.log(s.get().w);
    console.log(s.get().h);
    if (cb.ui.multi.length > 1 && (s.get().w <= MIN_SELECTION_WIDTH || s.get().h <= MIN_SELECTION_HEIGHT)) {
        ClearSelection();  
    } else {
        s.cropBox.Update(null,null,null,null,s.get());
    }
}

function showAjax(text) {
    $.mobile.loading( 'show', {
        text: text,
        textVisible: true,
        theme: 'z',
        html: ""
    });
}

function hideAjax() {
    $.mobile.loading( 'hide' );
}

$( document ).bind( 'mobileinit', function(){
  $.mobile.loader.prototype.options.text = "";
  $.mobile.loader.prototype.options.textVisible = true;
  $.mobile.loader.prototype.options.theme = "a";
  $.mobile.loader.prototype.options.html = "";
});


