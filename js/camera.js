var viewport = {
    width : $(window).width(),
    height : $(window).height()
};
    
function onStartCamera() {
    var tapEnabled = true;
    var dragEnabled = true;
    var toBack = true;
    
    cordova.plugins.camerapreview.startCamera({x: 0, y: 0, width: viewport.width, height: viewport.height}, "back", tapEnabled, dragEnabled, toBack);
};
    
function onStopCamera() {
    cordova.plugins.camerapreview.stopCamera();
};

function onTakePicture() {
    showAjax('Capturando Foto...');
    cordova.plugins.camerapreview.takePicture({maxWidth:640, maxHeight:640});
};

function onSwitchCamera() {
    cordova.plugins.camerapreview.switchCamera();
};
    
function onShow() {
    cordova.plugins.camerapreview.show();
};
    
function onHide() {
    cordova.plugins.camerapreview.hide();
};

function onColorEffectChanged() {
    var effect = document.getElementById('imageColorEffect').value;
    cordova.plugins.camerapreview.setColorEffect(effect);
};
    
function onZoomIn() {
    cordova.plugins.camerapreview.zoomIn();
};

function onZoomOut() {
    cordova.plugins.camerapreview.zoomOut();
};

function onSearchPhotoSucess(imageURI) {
    photoURI_Extraction = imageURI;
    jQuery(document).trigger('searchPhotoEvent', null);
}

function onSearchPhotoFail() {
    
}
                          
function onSearchPhoto() {    
    navigator.camera.getPicture(onSearchPhotoSucess, onSearchPhotoFail, {
        quality         : 100,
        destinationType : navigator.camera.DestinationType.FILE_URI,
        sourceType      : navigator.camera.PictureSourceType.PHOTOLIBRARY
    });
};